{
  description = "hscnvim flake";

  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
    treefmt-nix.url = "github:numtide/treefmt-nix";
  };

  outputs = inputs @ {
    devshell,
    flake-parts,
    pre-commit-hooks-nix,
    treefmt-nix,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        devshell.flakeModule
        pre-commit-hooks-nix.flakeModule
        treefmt-nix.flakeModule
      ];

      perSystem = {
        config,
        lib,
        pkgs,
        ...
      }: {
        devshells.default = {
          commands = let
            source = ["lua"];
          in [
            {
              name = "reload";
              help = "manually rebuild the shell environment";
              command = ''
                nix-direnv-reload
              '';
            }
            {
              name = "format";
              help = "run automated code formatting on the project";
              command = ''
                treefmt
              '';
            }
            {
              name = "doc";
              help = "generate documentation for the project";
              command = ''
                ${pkgs.luajitPackages.ldoc}/bin/ldoc lua -c .ldoc .
              '';
            }
            {
              name = "check";
              help = "run automated checks on the project";
              command = let
                inherit (lib.strings) concatStringsSep;
              in ''
                ${pkgs.luajitPackages.luacheck}/bin/luacheck ${concatStringsSep " " source}
              '';
            }
          ];
          devshell.startup.preCommit = {
            text = config.pre-commit.installationScript;
          };
          packages = with pkgs;
            [
              luajit
            ]
            ++ (with luajitPackages; [
              ldoc
              luacheck
            ]);
          packagesFrom = [
            config.treefmt.build.devShell
          ];
          serviceGroups = {
          };
        };
        pre-commit = {
          settings = {
            default_stages = ["pre-commit"];
            hooks = {
              commitizen.enable = true;
              deadnix = {
                enable = true;
                settings = {
                  noUnderscore = true;
                };
              };
              treefmt = {
                enable = true;
                extraPackages = with pkgs; [
                  alejandra
                  stylua
                ];
              };
            };
          };
        };
        treefmt = {
          programs = {
            alejandra.enable = true;
            stylua.enable = true;
          };
          projectRootFile = "flake.nix";
        };
      };
      systems = ["x86_64-linux"];
    };
}
