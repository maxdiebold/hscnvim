It's not clear what the exact content and functionality of your `hsc3.el` file are since it seems to be an Emacs Lisp script related to SuperCollider (`hsc3` likely indicates Haskell SuperCollider), and you're looking to convert it to Lua for use in Neovim. Without seeing the specific Emacs Lisp functionalities you're using, a direct line-by-line conversion can't be provided here.

However, I can guide you through a general approach to rewriting an Emacs Lisp script for Neovim in Lua, focusing on common scripting tasks that might relate to interfacing with SuperCollider or similar audio synthesis/processing environments. 

### Step 1: Analyze the Emacs Lisp Script
Determine the script's functionality. Common tasks might include:
- Sending commands to SuperCollider 
- Editing SuperCollider `.scd` files
- Interacting with the Emacs interface, e.g., displaying buffers or messages

### Step 2: Familiarize with Neovim Lua API
Lua in Neovim has a robust API that allows you to interact with the editor and perform complex tasks. Key areas include:
- `vim.api`: Functions for directly interacting with Neovim (e.g., manipulating buffers, windows)
- `vim.ui`: For user interface related tasks
- `vim.fn`: Calling Vimscript functions from Lua
- `vim.cmd`: Executing Vim commands
- `vim.loop`: For asynchronous tasks, which might be useful for interfacing with external processes like SuperCollider

### Step 3: Identify Equivalent Lua Functions
Map out equivalents for your tasks. For example:
- If your Emacs script sends commands to a SuperCollider server, you might use Neovim's ability to run shell commands or interact with external processes with `vim.fn.system()` or `vim.loop.spawn()`.
- For manipulating `.scd` files or data, Neovim's Lua API provides functions to read from, write to, and manipulate buffers.
- UI interactions, such as opening windows or displaying messages, can be achieved through `vim.api.nvim_open_win()` or `vim.api.nvim_echo()`.

### Step 4: Rewrite the Script in Lua
Start converting the logic of your Emacs Lisp script into Lua, keeping in mind the differences in syntax and the Lua API's capabilities. 

For example, consider an Emacs Lisp function that sends a "play" command to SuperCollider:

```lisp
(defun play-super-collider ()
  (interactive)
  (shell-command "sclang play.scd"))
```

A basic conversion to Lua for Neovim might look like this:

```lua
function play_super_collider()
  vim.fn.system("sclang play.scd")
end
```

Or, utilizing Neovim's asynchronous capabilities:

```lua
local uv = vim.loop

function play_super_collider_async()
  local handle, pid = uv.spawn("sclang", {
    args = {"play.scd"},
    stdio = {nil, nil, nil} -- stdin, stdout, stderr
  }, function(code, signal) -- on exit
    uv.close(handle)
    print("Finished with code", code)
  end)
end
```

### Step 5: Testing and Tweaking
Lua scripts can be loaded and executed in Neovim using the `:lua` command or sourcing a Lua file with `:source`. Test your rewritten script extensively, tweaking as needed to ensure it behaves as expected.

### Remember:
Directly converting complex Emacs Lisp logic to Lua can be challenging because of differences in the environments and available libraries. For specific functionalities, you might need to explore Neovim plugins or external Lua libraries, or even write more complex wrapper functions to achieve the same behavior.
