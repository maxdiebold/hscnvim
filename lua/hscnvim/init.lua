--- Main module.
---@module hscnvim
---@author J Max Diebold
---@license GPLv3

local L = {}
local M = {}

local auto_import_modules = false -- Flag to decide if the hsc3 standard module set is imported on startup
-- These can be fetched from the SuperCollider archives with the command:
-- `git checkout 2495f4f61c0955001169d28142543aabbe1bedcb build/old_rtf_help.tar.gz`
local help_directory = nil -- The directory containing the Sc3 RTF help files
local hsc3_directory = nil -- The hsc3 directory
local interpreter = { 'ghci' } -- The name of the Haskell interpreter
local proc = nil -- The id of the hsc3 Haskell process
local draw_command = 'draw' -- The un-qualified name of the draw function to use at `draw_region`

--- Change directory at ghci to current value of `default_directory`.
M.cd = function()
  L.send_line(string.format(':cd "%s"', vim.fn.expand '%:p:h'))
end

--- Check if the process with id `id` has finished.
--- @param id number? The process id.
L.check_proc = function(id)
  if id == nil then
    return false
  end
  return vim.fn.jobwait({ id }, 0)[1] == -1
end

--- Split the string `s` into chunks of `n` characters.
--- @param n number The number of characters per chunk.
--- @param s string The string to split.
L.chunk_string = function(n, s)
  local t = {}
  for i = 1, #s, n do
    table.insert(t, s:sub(i, i + n - 1))
  end
  return t
end

--- Find files at directory `dir` matching the regular expression `rgx`.
--- @param dir string The directory to search.
--- @param rgx string The regular expression to match.
L.find_files = function(dir, rgx)
  local files = vim.fn.findfile(rgx, dir)
  for _, file in ipairs(files) do
    vim.cmd('edit ' .. file)
  end
end

--- Lookup up the name at point in the hsc3 help files.
M.help = function()
  assert(hsc3_directory, 'hsc3 directory not set')
  local rgx = '^' .. vim.fn.expand '<cword>' .. '\\.help\\.l?hs$'
  L.find_files(vim.fn.join({ hsc3_directory, 'Help' }, '/'), rgx)
end

--- Send standard set of hsc3 and related module imports to haskell.
M.import_standard_modules = function()
  local file = hsc3_directory .. '/lib/hsc3-std-imports.hs'
  local f = io.open(file, 'r')
  if f == nil then
    error('File not found: ' .. file)
  end
  for line in f:lines() do
    L.send_line(line)
  end
  f:close()
end

--- Insert `text` after the current word.
--- @param text string The text to insert.
L.insert_text_after_cword = function(text)
  -- Retrieve the current buffer and cursor position
  local bufnr = vim.api.nvim_get_current_buf()
  local row, col = unpack(vim.api.nvim_win_get_cursor(0))
  -- Adjust the row and col to 0-index for API usage.
  row = row - 1
  -- Get the current line's content.
  local line = vim.api.nvim_buf_get_lines(bufnr, row, row + 1, false)[1]
  -- Find the end of the word under cursor using pattern matching.
  -- Adjust 'col' because Lua string indexing starts at 1.
  local _, word_end_col = line:sub(1, col + 1):find '%w+%W'
  -- If a word is found, calculate the position to insert the text.
  -- Otherwise, append at the end of the line.
  local insert_pos = word_end_col or #line + 1
  -- Insert the text after the word under cursor.
  -- Adjust insert position back to 0-indexing for the API.
  local new_line = line:sub(1, insert_pos - 1) .. text .. line:sub(insert_pos)
  vim.api.nvim_buf_set_lines(bufnr, row, row + 1, false, { new_line })
end

--- Send `:load` and the current buffer file name to Haskell.
M.load_current_file = function()
  vim.cmd 'write'
  M.see_haskell()
  L.send_line(string.format(':load "%s"', vim.fn.expand '%:p'))
end

--- Delete trailing newlines from `s`.
L.remove_trailing_newline = function(s)
  return string.gsub(s, '\n$', '')
end

--- Lookup up the name at point in the Sc3 (RTF) help files.
M.sc3_help = function()
  assert(help_directory, 'Sc3 help directory not set')
  local rgx = '^' .. vim.fn.expand '<cword>' .. '\\(\\.help\\)?\\.rtf$'
  L.find_files(help_directory, rgx)
end

--- Lookup up the Ugen name at point in the Sc3 (SCDOC) help files.
M.sc3_help_scdoc = function()
  L.send_line(
    string.format(
      'Sound.Sc3.Common.Help.sc3_scdoc_help_open False (Sound.Sc3.Common.Help.sc3_scdoc_help_path (Sound.Sc3.Ugen.Db.ugen_sc3_name "%s"))',
      vim.fn.expand '<cword>'
    )
  )
end

--- Show haskell output.
M.see_haskell = function()
  if not L.check_proc(proc) then
    L.start_haskell()
  end
  vim.cmd 'split'
  --(with-current-buffer hsc3-buffer
  --  (let ((window (display-buffer (current-buffer))))
  --    (goto-char (point-max))
  --    (save-selected-window
  --      (set-window-point window (point-max)))))))
end

--- Send the string `s` to Haskell using ghci layout block notation.
--- @param s string The string to send.
L.send_layout_block = function(s)
  L.send_line(vim.fn.join({ ':{', s, ':}' }, '\n'))
end

--- Send the string `s`, with newline appended, to Haskell.
--- @param s string The string to send.
L.send_line = function(s)
  if L.check_proc(proc) then
    local cs = L.chunk_string(64, s .. '\n')
    for _, c in ipairs(cs) do
      vim.fn.chansend(proc, c)
    end
  else
    error 'No hsc3 process'
  end
end

--- Send the `:quit` instruction to Haskell.
M.send_quit = function()
  L.send_line ':quit'
end

--- If the string `s` spans multiple lines `send_layout_block` else `send_line`.
--- @param s string The string to send.
L.send_text = function(s)
  local has_newline = string.match(s, '\n')
  if has_newline then
    L.send_layout_block(s)
  else
    L.send_line(s)
  end
end

--- Send string `s` with Haskell function `fn` applied using the `$` operator.
--- If `s` has a newline the layout is adjusted accordingly.
--- @param fn string The Haskell function to apply.
--- @param s string The string to send.
L.send_text_fn = function(fn, s)
  local has_newline = string.match(s, '\n')
  local text = has_newline and (fn .. ' $\n' .. s) or (fn .. ' $ ' .. s)
  L.send_text(text)
end

--- Set ghci prompt to hsc3> and the continuation prompt to nil.
M.set_prompt = function()
  L.send_line ':set prompt "hsc3> "'
  L.send_line ':set prompt-cont ""'
end

--- Start the hsc3 haskell process.
--- If `interpreter` is not already a subprocess it is started and a new window is created to display the results of evaluating hsc3 expressions.  Input and output is via `proc`.
M.start_haskell = function()
  if L.check_proc(proc) then
    M.see_haskell()
  else
    proc = vim.fn.jobstart(interpreter, { rpc = true })
    M.set_prompt()
    if auto_import_modules then
      M.import_standard_modules()
    end
    M.see_haskell()
  end
end

--- Insert control Ugen parameters (arguments) for the Ugen before point.
M.ugen_control_param = function()
  local cmd = string.format('help ugen exemplar hs/control %s', vim.fn.expand '<cword>')
  local res = vim.fn.system(cmd)
  L.insert_text_after_cword(' ' .. L.remove_trailing_newline(res))
end

--- Insert control Ugen parameters (arguments) for the Ugen before point."
M.ugen_control_param_let = function()
  local cmd = string.format('help ugen exemplar hs/control/let %s', vim.fn.expand '<cword>')
  local res = vim.fn.system(cmd)
  L.insert_text_after_cword(' ' .. L.remove_trailing_newline(res))
end

--- Insert the default Ugen parameters (arguments) for the Ugen before point."
M.ugen_default_param = function()
  local cmd = string.format('help ugen exemplar hs %s', vim.fn.expand '<cword>')
  local res = vim.fn.system(cmd)
  L.insert_text_after_cword(' ' .. L.remove_trailing_newline(res))
end

--- Lookup up the Ugen at point in hsc3-db.
M.ugen_summary = function()
  L.send_line(string.format('Sound.Sc3.Ugen.Db.Pp.ugen_summary_wr "%s"', vim.fn.expand '<cword>'))
end

--- Remove initial comment and Bird-literate markers from the string S if present.
--- @param s string The string to uncomment.
L.uncomment = function(s)
  return string.gsub(s, '^[- ]*[> ]*', '')
end

--- Get current line.
L.current_line = function()
  return vim.api.nvim_get_current_line()
end

--- Send the current line to haskell.
M.send_current_line = function()
  L.send_line(L.uncomment(L.current_line()))
end

--- Send main to haskell.
M.send_main = function()
  L.send_line 'main'
end

--- Set the mark at the start and point at the end of the current paragraph.
M.set_region_to_paragraph = function()
  vim.cmd 'normal! {'
  vim.cmd 'normal! v'
  vim.cmd 'normal! }'
end

--- Get current region as string.
L.region_string = function()
  return table.concat(vim.api.nvim_buf_get_lines(0, vim.fn.line 'v', vim.fn.line '.', false), '\n')
end

--- Send region text to interpreter.
M.send_region = function()
  L.send_text(L.uncomment(L.region_string()))
end

--- Send region text with haskell function `fn` to be applied.
--- @param fn string The Haskell function to apply
L.send_region_fn = function(fn)
  L.send_text_fn(fn, L.uncomment(L.region_string()))
end

--- Play region at scsynth. The (one-indexed) prefix argument `k` indicates which server to send to.
--- @param k number The server index
M.play_region_at = function(k)
  L.send_region_fn(
    string.format('Sound.Sc3.auditionAt ("%s",%d + %d) Sound.Sc3.def_play_opt', L.server_host(), L.server_port(), k)
  )
end

--- The host on which `scsynth` is listening.
L.server_host = function()
  return os.getenv 'SC_HOST' or '127.0.0.1'
end

--- The port at which `scsynth` is listening.
L.server_port = function()
  return os.getenv 'SC_PORT' or 57110
end

--- Play region at `scsynth`.
M.play_region = function()
  L.send_region_fn 'Sound.Sc3.audition'
end

L.call_region_fn = function(fn)
  local initial_pos = vim.api.nvim_win_get_cursor(0)
  M.set_region_to_paragraph()
  fn()
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Esc>', true, false, true), 'n', false)
  vim.api.nvim_win_set_cursor(0, initial_pos)
end

M.play = function()
  L.call_region_fn(M.play_region)
end

--- Play region at scsynth using stateful interface.
M.play_region_stateful = function()
  L.send_region_fn 'Sound.Sc3.scSynthPlay scSynth'
end

--- Draw region Ugen graph.
M.draw_region = function()
  L.send_region_fn(string.format('Sound.SC3.UGen.Dot.%s $ wrapOut Nothing', draw_command))
end

--- Draw Ugen graph.
M.draw = function()
  L.call_region_fn(M.draw_region)
end

return M
